==== Meshes: 1
- Mesh 'Mesh' vtx:8 face:6 loop:24 edge:12
    - 0 2 3 1 4 ... 7 2 0 4 6 
    - 0/2 2/3 1/3 0/1 4/5 ... 4/6 1/5 0/4 3/7 2/6 
  - attr 'position' FLOAT_VECTOR POINT
    - (-10.000, -10.000, 0.000)
    - (10.000, -10.000, 0.000)
    - (-10.000, 10.000, 0.000)
      ...
    - (10.000, -10.000, 20.000)
    - (-10.000, 10.000, 20.000)
    - (10.000, 10.000, 20.000)
  - attr 'sharp_edge' BOOLEAN EDGE
    - 1 1 1 1 1 ... 1 1 1 1 1 
  - attr 'material_index' INT FACE
    - 0 0 0 0 0 0 
  - attr 'custom_normal' INT16_2D CORNER
    - (0, 0)
    - (0, 0)
    - (0, 0)
      ...
    - (0, 0)
    - (0, 0)
    - (0, 0)
  - attr 'UVChannel_1' FLOAT2 CORNER
    - (1.000, 0.000)
    - (1.000, 1.000)
    - (0.000, 1.000)
      ...
    - (1.000, 0.000)
    - (1.000, 1.000)
    - (0.000, 1.000)
  - 1 materials
    - '01 - Default' 

==== Objects: 1
- Obj 'Box001' MESH data:'Mesh'
  - pos 0.000, 0.000, 0.000
  - rot 0.000, 0.000, 0.000 (XYZ)
  - scl 0.025, 0.025, 0.025
  - props: int:MaxHandle=1

==== Materials: 1
- Mat '01 - Default'
  - base color (0.010, 0.020, 0.030)
  - specular ior 1.000
  - specular tint (1.000, 1.000, 1.000)
  - roughness 0.553
  - metallic 0.000
  - ior 1.500
  - alpha 0.953
  - emission color (0.090, 0.100, 0.110)
  - emission strength 1.000
  - viewport diffuse (0.010, 0.020, 0.030, 1.000)
  - viewport specular (1.000, 1.000, 1.000), intensity 1.000
  - viewport metallic 0.000, roughness 0.553
  - backface False probe True shadow False

==== Images: 10
- Image 'Map #1' 128x128 32bpp
- Image 'Map #12' 128x128 32bpp
- Image 'Map #15' 128x128 32bpp
- Image 'Map #17' 128x128 32bpp
- Image 'Map #2' 128x128 32bpp
- Image 'Map #3' 128x128 32bpp
- Image 'Map #4' 128x128 32bpp
- Image 'Map #5' 128x128 32bpp
- Image 'Map #7' 128x128 32bpp
- Image 'Map #8' 128x128 32bpp

