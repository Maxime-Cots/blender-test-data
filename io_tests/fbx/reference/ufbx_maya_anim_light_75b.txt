==== Objects: 1
- Obj 'pointLight1' LIGHT data:'Light'
  - pos 0.000, 0.000, 0.000
  - rot 0.000, 0.000, 0.000 (XYZ)
  - scl 0.010, 0.010, 0.010
  - anim act:pointLight1|Take 001|BaseLayer slot:OBpointLight1|Take 001|BaseLayer blend:REPLACE drivers:0

==== Lights: 1
- Light 'Light' POINT col:(0.148, 0.095, 0.440) energy:3.072

==== Actions: 1
- Action 'pointLight1|Take 001|BaseLayer' curverange:(0.0 .. 0.0) curves:9
  - fcu 'location[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:0
  - fcu 'location[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:0
  - fcu 'location[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:0
  - fcu 'rotation_euler[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:0
  - fcu 'rotation_euler[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:0
  - fcu 'rotation_euler[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:0
  - fcu 'scale[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:0
  - fcu 'scale[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:0
  - fcu 'scale[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:0

